package de.e2.sps.web

import de.e2.sps.start.Spiel
import de.e2.sps.start.Wahl
import de.e2.sps.start.Wahl.*
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.http.ContentType
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.ButtonType
import kotlinx.html.FORM
import kotlinx.html.FormMethod
import kotlinx.html.body
import kotlinx.html.button
import kotlinx.html.form
import kotlinx.html.h1
import kotlinx.html.label

fun string2Wahl(input: String): Wahl? {
    return when (input.toLowerCase()) {
        "stein" -> STEIN
        "papier" -> PAPIER
        "schere" -> SCHERE
        else -> null
    }
}

fun String.toWahl(): Wahl? = //this ist String
    when (this.toLowerCase()) {
        "stein" -> STEIN
        "papier" -> PAPIER
        "schere" -> SCHERE
        else -> null
    }


fun main() {
    println("Open with: http://localhost:8080/spiel?wahl=papier")
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/spiel") {
                val spielerWahl = call.parameters["wahl"]?.toWahl()
                if (spielerWahl == null) {
                    call.respondText("Ungültige Wahl!")
                } else {
                    val computerWahl = values().random()
                    val spiel = Spiel(spielerWahl, computerWahl)

                    call.respondText(
                        """
                        <h1>Stein Papier Schere</h1>    
                        <p>Spieler hat gewählt ${spiel.spielerWahl}.</p>
                        <p>Computer hat gewählt ${spiel.computerWahl}.</p>
                        <p>Das Ergebnis ist: ${spiel.ermittleErgebnis()}.</p>
                        """.trimIndent(), ContentType.Text.Html
                    )
                }
            }


            get("/start") {
                call.respondHtml {
                    body {
                        h1 { text("Stein Papier Schere") }
                        form(action = "/spiel", method = FormMethod.get) {
                            // this ist FORM
                            label { +"Deine Wahl:" }
                            wahlButton("Stein")
                            wahlButton("Papier")
                            wahlButton("Schere")
                        }
                    }
                }
            }
        }
    }
    println("Open with: http://localhost:8080/start")
    server.start(wait = true)
}

fun FORM.wahlButton(wahl: String) {
    button(type = ButtonType.submit, name = "wahl") {
        value = wahl.toLowerCase()
        +wahl
    }
}