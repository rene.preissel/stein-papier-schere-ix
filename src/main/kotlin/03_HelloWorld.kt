import io.ktor.application.call
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty


fun main() {
    println("Open with: http://localhost:8080/hello?count=3")
    val server = embeddedServer(factory = Netty, port = 8080) {
        //this ist Application
        routing {
            //this ist Routing
            get("/hello") {
                //this ist PipelineContext
                val countAsString: String? = call.parameters["count"]
                val count = if (countAsString != null) countAsString.toInt() else 3
                call.respondTextWriter {
                    //this ist Writer
                    repeat(count) { index ->
                        write("Hello World\n")
                    }
                }
            }
        }
    }
    server.start(wait = true)
}


