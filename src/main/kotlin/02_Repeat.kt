fun repeat(times: Int, action: (Int) -> Unit) {
    for (index in 1..times) {
        action(index)
    }
}

fun printNumber(number: Int) {
    println(number)
}

fun main() {
    repeat(3, ::printNumber)

    repeat(3) { number ->
        println(number)
    }

    repeat(3) {
        println(it)
    }
}