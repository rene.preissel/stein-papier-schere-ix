package de.e2.sps.start

import de.e2.sps.start.Ergebnis.*
import de.e2.sps.start.Wahl.*

enum class Ergebnis {
    SPIELER_GEWINNT, COMPUTER_GEWINNT, UNENTSCHIEDEN
}

enum class Wahl {
    STEIN, PAPIER, SCHERE
}

class Spiel(val spielerWahl: Wahl, val computerWahl: Wahl) {
    fun ermittleErgebnis(): Ergebnis {
        return when {
            spielerWahl == computerWahl -> UNENTSCHIEDEN
            spielerWahl == PAPIER && computerWahl == STEIN -> SPIELER_GEWINNT
            spielerWahl == STEIN && computerWahl == SCHERE -> SPIELER_GEWINNT
            spielerWahl == SCHERE && computerWahl == PAPIER -> SPIELER_GEWINNT
            else -> COMPUTER_GEWINNT
        }
    }
}

fun main() {
    val spiel = Spiel(STEIN, PAPIER)
    val spielerWahl = spiel.spielerWahl
    println("Spieler: $spielerWahl. Computer: ${spiel.computerWahl}.")
    println("Ergebnis: ${spiel.ermittleErgebnis()}.")
}