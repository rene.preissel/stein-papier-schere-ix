data class Rechteck(val breite: Int, val hoehe: Int): Comparable<Rechteck> {
    val flaeche: Int = breite * hoehe

    override fun compareTo(other: Rechteck): Int = this.flaeche - other.flaeche
}

fun main() {
    val r1 = Rechteck(10, 20)
    val r2 = r1.copy(hoehe = 30)
    println(r1)       // Rechteck(breite=10, hoehe=20)
    println(r1 == r2) // false
}