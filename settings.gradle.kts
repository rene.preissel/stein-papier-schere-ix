rootProject.name = "stein-papier-schere-ix"

pluginManagement {
    plugins {
        val kotlinVersion = "1.3.70"

        id("org.jetbrains.kotlin.jvm") version kotlinVersion
        id("org.jetbrains.kotlin.plugin.serialization") version kotlinVersion
    }
}

